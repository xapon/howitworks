# Как это работает

Изучаем производительность различных операций, выполняя

```
sudo perf stat php path-to-file.php
```

Например, что быстрее - объекты или массивы?

```
sudo perf stat php array-vs-object/arrays.php
sudo perf stat php array-vs-object/objects.php
```

## Установка perf

```
sudo apt-get install linux-tools-common linux-tools-generic linux-tools-`uname -r`
```

На виртуальных машинах работают не все метрики!