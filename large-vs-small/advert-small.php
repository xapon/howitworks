<?php
class AdvertId {
    public $id;
    public $storage;

    public function __construct($id, $storage)
    {
        $this->id = $id;
        $this->storage = $storage;
    }
}