<?php
// работаем с большими классами - выполняем 1 цикл по всем объектам
require_once 'advert-large.php';

// инициализация не учитывается в измерениях
$adverts = [];
for ($i = 0; $i < 1000000; $i++) {
    $adverts[] = new Advert($i, "live");
}

$start = microtime(true);

workWithAdverts($adverts);

$end = microtime(true);
$time = $end - $start;
echo $time ."s\n";

/**
 * @param Advert[] $adverts
 */
function workWithAdverts(array $adverts) {
    foreach ($adverts as $advert) {
        deleteAdvert($advert);
    }
}

function deleteAdvert(Advert $advert) {
    // deleting advert
}