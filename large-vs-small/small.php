<?php
// работаем с компактными классами - выполняем 1 цикл по всем объектам
require_once 'advert-small.php';

// инициализация не учитывается в измерениях
$ids = [];
for ($i = 0; $i < 1000000; $i++) {
    $ids[] = new AdvertId($i, "live");
}

$start = microtime(true);

workWithAdvertIds($ids);

$end = microtime(true);
$time = $end - $start;
echo $time ."s\n";

/**
 * @param AdvertId[] $ids
 */
function workWithAdvertIds(array $ids) {
    foreach ($ids as $id) {
        deleteById($id);
    }
}

function deleteById(AdvertId $id) {
    // deleting advert by id
}