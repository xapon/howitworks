<?php
class Advert {
    public $id;
    public $storage;
    public $serviceData;
    public $systemData;
    public $data;
    public $category;
    public $version;
    public $views;
    public $logs;
    public $photos;
    public $isModified;
    public $moderatorLogin;
    public $something;
    public $else;
    public $so;
    public $many;
    public $properties;
    public $here;

    public function __construct($id, $storage)
    {
        $this->id = $id;
        $this->storage = $storage;
    }
}
