<?php
// работаем с компактными классама, собранными из больших - выполняем 5 циклов по всем объектам
require_once 'advert-large.php';
require_once 'advert-small.php';

// инициализация не учитывается в измерениях
$adverts = [];
for ($i = 0; $i < 1000000; $i++) {
    $adverts[] = new Advert($i, "live");
}

$start = microtime(true);

// конвертация больших объектов в компактные _учитывается_ в измерениях
$ids = [];
foreach ($adverts as $advert) {
    $ids[] = new AdvertId($advert->id, $advert->storage);
}

workWithAdverts($ids);
workWithAdverts($ids);
workWithAdverts($ids);
workWithAdverts($ids);
workWithAdverts($ids);

$end = microtime(true);
$time = $end - $start;
echo $time ."s\n";





/**
 * @param AdvertId[] $ids
 */
function workWithAdverts(array $ids) {
    foreach ($ids as $id) {
        deleteAdvert($id);
    }
}

function deleteAdvert(AdvertId $id) {
    // deleting advert
}