<?php
$counter = 0;
$tmp = 0;

for ($i = 0; $i < 10000000; $i++) {
    $tmp = rand(0, 1);

    if ($tmp === 0) {
        $counter++;
    } else {
        $counter--;
    }
}

echo $counter . "\n";