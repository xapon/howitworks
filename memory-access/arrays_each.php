<?php
require_once 'classes.php';

$data = ['a' => ['b' => ['c' => 0]]];

for ($i = 0; $i < 10000000; $i++) {
    $data['a']['b']['c'] += 1;
}

echo $data['a']['b']['c'] . "\n";