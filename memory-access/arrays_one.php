<?php
require_once 'classes.php';

$data = ['a' => ['b' => ['c' => 0]]];
$tmp = 0;

for ($i = 0; $i < 10000000; $i++) {
    $tmp += 1;
}

$data['a']['b']['c'] = $tmp;

echo $data['a']['b']['c'] . "\n";