<?php

class A {
    public $b;
    protected static $instance;

    public function __construct()
    {
        $this->b = new B();
    }

    public static function getDefault() {
        if (!self::$instance) {
            self::$instance = new A();
        }

        return self::$instance;
    }

    public function getB()
    {
        return $this->b;
    }
}

class B {
    public $c;


    public function __construct()
    {
        $this->c = new C();
    }

    public function getC()
    {
        return $this->c;
    }
}

class C {
    public $d = 1;

    public function getD()
    {
        return $this->d;
    }
}