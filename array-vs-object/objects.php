<?php
class Advert {
    public $id;
    public $title;
    public $category;

    public function __construct($id, $title, $category)
    {
        $this->id = $id;
        $this->title = $title;
        $this->category = $category;
    }
}

$adverts = [];

for ($i = 0; $i < 1000000; $i++) {
    $adverts[] = new Advert($i, 'hello', 'sell.flat');
}

echo count($adverts) . "\n";

echo (memory_get_peak_usage(true) / 1024 / 1024) . "mb\n";