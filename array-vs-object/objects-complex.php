<?php
class Advert {
    public $id;
    public $title;
    public $category;

    public $additional;

    public function __construct($id, $title, $category)
    {
        $this->id = $id;
        $this->title = $title;
        $this->category = $category;
        // 200-символьная строка компенсирует экономию при переходе с массивов
        $this->additional = str_repeat("a", 200);
    }

    // методы никак не влияют на потребление памяти
    public function a() {
        return $this->title . " " . $this->category;
    }
    public function b() {}
    public function c() {}
    public function d() {}
    public function e() {}
    public function f() {}
    public function g() {}
}

$adverts = [];


for ($i = 0; $i < 1000000; $i++) {
    $adverts[] = new Advert($i, 'hello', 'sell.flat');
}

echo count($adverts) . "\n";

echo (memory_get_peak_usage(true) / 1024 / 1024) . "mb\n";