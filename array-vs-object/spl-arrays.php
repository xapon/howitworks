<?php
$adverts = new \SplFixedArray(1000000);

for ($i = 0; $i < 1000000; $i++) {
    $tmp = new \SplFixedArray(3);
    $tmp[0] = $i;
    $tmp[1] = 'hello';
    $tmp[2] = 'sell.flat';

    $adverts[$i] = $tmp;
}

echo count($adverts) . "\n";

echo (memory_get_peak_usage(true) / 1024 / 1024) . "mb\n";